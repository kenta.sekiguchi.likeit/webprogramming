package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class NewlegisterServlet
 */
@WebServlet("/NewlegisterServlet")
public class NewlegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewlegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Newlegister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        
		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name= request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String checkpass=request.getParameter("checkpass");
		 
		if(!password.equals(checkpass)){
			Error error=new Error("入力された内容は正しくありません");
			request.setAttribute("message",error );
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Newlegister.jsp");
			dispatcher.forward(request, response);
		}else {
		
		UserDao dao=new UserDao();
		//ArrayList<UserDao> daolist=new ArrayList<>(); 
		
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		dao.Insertpalam(loginId,name,birthDate,password);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

		}
				
	}
}
