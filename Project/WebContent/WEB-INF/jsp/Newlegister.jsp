<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>
        <div class="p-2 mb-2 bg-secondary text-white row">
            <div class="col-sm-8 text-sm-right">${userInfo.name} さん</div>
            <div class="col-sm-4 text-sm-right"><a href="LogoutServlet" class="text-danger" >ログアウト</a></div>
        </div>
    <div class="container">
    	<div class="col-sm-5 mx-auto">
        	<h1 style="font-size: 60px; color: black;">ユーザ新規登録</h1>
        </div>
        <div class="text-danger text-center">
        ${message}
        </div>
        <br><br><br><br>
<!--＊＊＊＊＊＊＊ここからフォーム＊＊＊＊＊＊＊-->
    <form action="NewlegisterServlet" method="post">
        <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="1"  class="col-sm-5 col-form-label text-sm-right">
                ログインID　　
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5">
						<input type="text" name="loginId" class="form-control" id="1" required>
					</div>
            </div>
        </div>
  <div class="form-group row">
    <label style="font-size: 20px; color: black;" for="2" class="col-sm-5 col-form-label text-sm-right">
        パスワード　　 
    </label>
        <div class="col-sm-7">
        <div class="col-sm-6 ml-5">
            <input type="password" name="password" class="form-control" id="2" required >
        </div>
        </div>
  
            <label style="font-size: 20px; color: black;" for="3"  class="col-sm-5 col-form-label text-sm-right">
            パスワード(確認)
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5">
                <input type="password" name="checkpass" class="form-control" id="3" oninput="CheckPassword(this)" required>
            </div>
            </div>
        </div>
        
            <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="4"  class="col-sm-5 col-form-label text-sm-right">
                ユーザ名　　　
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5">
                <input type="text" name="name" class="form-control" id="4" required>
            </div>
            </div>
        </div>
            <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="5"  class="col-sm-5 col-form-label text-sm-right">
                生年月日　　　
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5">
                <input type="date" name="birthDate" class="form-control" id="5" required>
            </div>
            </div>
        </div>
        <br>
<div class="col-md-2 col-md-offset-5 mx-auto">
    <input type="submit"  value="登録" style="width:120px">
</div>
    </form>
        <br><br>
        <div class="col-sm-4 text-sm-right"><a href="http://localhost:8070/UserManagement/UserListServlet" class="text-primary" >戻る</a></div>
    </div>
	</body>
</html>