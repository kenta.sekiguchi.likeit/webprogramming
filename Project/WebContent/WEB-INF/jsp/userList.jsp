<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>
        <div class="p-2 mb-2 bg-secondary text-white row">
            <div class="col-sm-8 text-sm-right">${userInfo.name} さん
            </div>
            <div class="col-sm-4 text-sm-right">
                <a href="LogoutServlet" class="text-danger" >ログアウト</a>
            </div>
        </div>
        <!--＊＊＊＊ここからメインテキスト＊＊＊＊-->
    <div class="container">   
　　　　    <div class="text-center">
            <h1 style="font-size: 60px; color: black;">ユーザ一覧</h1>
           </div>
    </div>    
        <br><br><br><br>
        <div class="text-sm-right"><a href="NewlegisterServlet" class="text-primary" >新規登録　</a></div>
        <!--＊＊＊＊ここからフォーム＊＊＊＊-->
<div class="container">   
    <form>
        <div class="form-group row">
            <label style="font-size:20px; color: black;" for="1" class=" col-sm-3 font-weight-bold">
                ログインID　　
            </label>
            <div class="col-sm-9">
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="1" >
                </div>
            </div>
        </div>
  <div class="form-group row">
    <label style="font-size: 20px; color: black;" for="2" class="col-sm-3 font-weight-bold">
        ユーザ名　　 
    </label>
        <div class="col-sm-9">
            <div class="col-sm-9">
                <input type="password" class="form-control" id="2" >
            </div>
        </div>
  </div>
        <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="3"  class="col-sm-3 font-weight-bold">
            生年月日
            </label>
            <div class="col-sm-9">
                <div class="col-sm-9">
                    <input type="date" id="3">　　　　　～<div class=float-right><input type="date"></div>
                </div>
            </div>
        </div>
<div class="col-md-2 float-right">
    <input type="button"  value="検索" class="w-100">
</div>
    </form>
        <br><br>
    <!--＊＊＊＊＊＊＊＊＊＊ここからテーブル＊＊＊＊＊＊＊＊＊＊＊-->
    <hr color="#999999">
<div class="table-responsive">
<table class="table table-bordered">
  <thead>
    <tr class="table-dark text-dark">
      <th width="15%">ログインID</th>
      <th width="15%">ユーザ名</th>
      <th width="30%">生年月日</th>
      <th width="40%"></th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="user" items="${userList}">
    <tr>
      <td>${user.loginId}</td>
      <td>${user.name}</td>
      <td>${user.birthDate}</td>
      <td>
        <div class="row">
        <div class="col-sm-4">
            <a class="w-100 text-white btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
        </div>
        <div class="col-sm-4">
            <a class="w-100 text-white btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
        </div>
        <div class="col-sm-4">
            <a class="w-100 text-white  btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
        </div>
        </div>
    </td>
    </tr>
    </c:forEach>
  </tbody>
</table>
</div>
</div>
	</body>
</html>