<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
        <div class="p-2 mb-2 bg-secondary text-white row">
            <div class="col-sm-8 text-sm-right">ユーザ名 さん</div>
            <div class="col-sm-4 text-sm-right"><a href="xxxx.html" class="text-danger" >ログアウト</a></div>
        </div>
    <div class="container">
        <div class="row">
　　　　    <div class="col-sm-6 mx-auto">
            <h1 style="font-size: 60px; color: black;">ユーザ情報詳細参照</h1>
           </div>
        </div>
        <br><br><br><br>
    <form>
        <div class="form-group row">
            <label style="font-size: 20px; color: black;" class="col-sm-5 col-form-label text-sm-right">
            ログインID　　　
            </label>
            <div class="col-sm-7" >
            <div class="col-sm-6 ml-5" style="font-size: 20px;" id="1">
                id0001
            </div>
            </div>
        </div>
  <div class="form-group row">
    <label style="font-size: 20px; color: black;" for="inputPassword" class="col-sm-5 col-form-label text-sm-right">
        ユーザ名　　 　
    </label>
        <div class="col-sm-7">
        <div class="col-sm-6 ml-5" style="font-size: 20px;">
                田中太郎
            </div>
        </div>
  </div>
        <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="staticEmail"  class="col-sm-5 col-form-label text-sm-right">
            生年月日　　　
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5" style="font-size: 20px;">
                1989年4月26日
            </div>
            </div>
        </div>
            <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="staticEmail"  class="col-sm-5 col-form-label text-sm-right">
                登録日時　　　
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5" style="font-size: 20px;">
                2017年01月01日 10:50
            </div>
            </div>
        </div>
            <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="staticEmail"  class="col-sm-5 col-form-label text-sm-right">
                更新日時　　　
            </label>
            <div class="col-sm-7">
            <div class="col-sm-6 ml-5" style="font-size: 20px;">
                2017年02月01日 01:05
            </div>
            </div>
        </div>
        <br>
<div class="col-md-2 col-md-offset-5 mx-auto">
    <input type="button"  value="登録" style="width:120px";>
</div>
    </form>
        <br><br>
        <div class="col-sm-4 text-sm-right"><a href="xxxx.html" class="text-primary" >戻る</a></div>
    </div>
	</body>
</html>