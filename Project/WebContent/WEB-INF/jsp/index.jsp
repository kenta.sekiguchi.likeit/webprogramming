<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container">   
　　　　    <div class="col-sm-5 mx-auto">
            <h1 style="font-size: 60px; color: black;">ログイン画面</h1>
           </div>
        <br><br><br><br>
        <!--＊＊＊＊＊＊＊＊＊ここからフォーム＊＊＊＊＊＊＊＊＊＊＊＊-->
    <form action="LoginServlet" method="post">
        <div class="form-group row">
            <label style="font-size: 20px; color: black;" for="1"  class="col-sm-4 col-form-label text-sm-right">
                ログインID
            </label>
            <div class="col-sm-8">
            	<div class="col-sm-6 ml-5">
                	<input type="text" name="loginId" class="form-control" id="1" placeholder="id">
            	</div>
            </div>
        </div>
  <div class="form-group row" >
    <label style="font-size: 20px; color: black;" for="2" class="col-sm-4 col-form-label text-sm-right">
        パスワード
    </label>
        <div class="col-sm-8">
        	<div class="col-sm-6 ml-5">
            	<input type="password" name="password" class="form-control" id="2" placeholder="Password">
       		</div>
        </div>
  </div>
        <br>
<div class="col-md-2 col-md-offset-5 mx-auto">
    <input type="submit" value="ログイン" style="width:120px";>
</div>
    </form>
    </div>
</body>
</html>